General structure
=================

The base and root o all other objects in th fib-o-mat package is the :class:`~fibomat.sample.Sample` class.
All other objects are added directly or indirectly to an instance of this class.

To a sample, :class:`~fibomat.site.Site` objects can be added. Sites have a
position and field of view as properties and :class:`~fibomat.pattern.Pattern`\ s can be added to a site.

A pattern is a collection of a patterning geometry/ies, beam settings and rasterizing settings.

A sample can have a unlimited number of sites. The same holds for sites and patterns.

The following example can used as a template for any project. ::

    from fibomat impotr Sample, Vector, U_

    sample = Sample()

    # add a site with position (5 µm, 6 µm) and a field of view of (2 µm, 2 µm)
    first_site = sample.create_site(
        dim_position = (Vector(5, 6), U_('µm'),
        dim_fov_ = (Vector(2, 2), U_('µm')
    )

    # see the following chapters to see how shapes, mills, and rasterizing_settings can be defined.
    first_site.create_pattern(
        dim_shape=...,
        mill=...,
        rasterizing_style=...
    )

    # plot the pattern design
    sample.plot()

    # export the pattern. See the corresponding chapter for more details.
    sample.export(...)

Transformation of sites and patterns
------------------------------------

Sites and patterns support the same transformations as shapes, introduced in the :ref:`next section <user_guide/geometric-shapes:rigid transformations and isotropic scaling>`. ...





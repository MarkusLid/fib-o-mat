Getting started
===============

An installed python version >= 3.8 is required.

Create a virtual environment and active it with

.. code-block:: bash

    $ python -m venv .venv
    # for *nix systems
    $ source .venv/bin/activate
    # for MS windows
    $ .venv\Scripts\activate.bat

See `here <https://docs.python.org/3/tutorial/venv.html>`__ for further information on virtual environments.

Install the fib-o-mat package with

.. code-block:: bash

    $ pip install --upgrade fibomat

If this does not work for you, fib-o-mat must be compiled for your machine. This is explained in the the
:doc:`User Guide <user_guide/installation>`.


First simple example
--------------------

This getting started guide shows how to create a simple patterning layout.
The layout will consist of a few line and spot shapes with different doses.

The complete python file can be found in the git's `example folder <https://gitlab.com/viggge/fib-o-mat/-/blob/master/examples/getting_started.py>`__.

Starting point of each patterning design is the :class:`~fibomat.sample.Sample` class. This class is the
gluing between everything else in this library. ::

    import numpy as np

    from fibomat import Sample, Pattern, Mill, U_, Q_
    from fibomat import shapes, raster_styles, linalg, default_backends


    dose_test_sample = Sample('an optional description for yourself')

Next, a patterning site must be defined. The patterning site defines the stage position of the microscope and the field
of view to be used during the patterning process.

(Physical) units, if needed, are specified with the help of the :mod:`fibomat.units` submodule. See the module
documentation for some examples. In the patterning site creation process, units are needed to specify the lengths units
for the stage position and the field of view. ::

    site = dose_test_sample.create_site(
        dim_position=([123, 456], U_('µm')), dim_fov=([5, 5], U_('µm')), description='another description'
    )

The site will be located at (123, 456) µm and will have a field of view of (5, 5) µm in horizontal and vertical direction respectively.
The interpretation of the `position` value is dependent on your microscope. For details, consult the documentation of
the used exporting backend (this concept will be introduced a little later).

The field of view (fov) parameter is optional. If it is not passed, it will be calculated automatically by the
bounding boxes of the contained pattern geometries (shapes), which will be added in the next step. This is currently not reliable and should not be used!

The library has some predefined shapes which can be used to build patterning geometries. A list of all shapes is given at :ref:`user_guide/geometric-shapes:geometric shapes`.

The pattering settings are specified with classes given in the :mod:`fibomat.mill` and :mod:`fibomat.raster_styles`
submodules.
Shapes and patterning settings are collected in class called :class:`~fibomat.pattern.Pattern`. It merges geometries, mill settings and some optional additional parameters.

Putting all together ::

    # create some spots
    spot = shapes.Spot([1, 2])
    site.create_pattern((spot, U_('µm')), single_repeat_mill, raster_style=raster_styles.zero_d.SingleSpot())

    spot = shapes.Spot([2, 2])
    site.create_pattern((spot, U_('µm')), single_repeat_mill, raster_style=raster_styles.zero_d.SingleSpot())

    # and a line shape
    line = shapes.Line(start=(-2, 2), end=(-1, 2))

    # a mill object with defines the dwell time per spot in the rasterized shape and the number of repeats
    mill = Mill(
        dwell_time=Q_('5 ms'), repeats=1
    )

    # and finally rasterizing style. In this case, the line will be rasterized consecutive from start to end.
    line_style = raster_styles.one_d.Curve(pitch=Q_('1 nm'), scan_sequence=raster_styles.ScanSequence.CONSECUTIVE)

    # everything is collected in a pattern
    line_pattern = Pattern(
        dim_shape=(line, U_('µm')),
        mill=mill,
        raster_style=line_style
    )

    # and added to the site.
    site += line_pattern
    # or
    # site.add_pattern(line_pattern)
    site += line_pattern
    # or
    # site.add_pattern(line_pattern)

Note that the ``spots``\ s and ``line`` are equipped with a length unit during pattern creation.

.. caution:: The shape position is interpreted always relative to the center of the corresponding site, not to the global coordinate system.

All shape elements can be translated, rotated scaled and mirrored. To add some rotated and translated lines, the following could
be done ::

    line = line.translated((0, -1))

    for i, angle in enumerate(np.linspace(0, np.pi/2, 4, endpoint=False)):
        # rotate the line around its center point and translate it
        rotated_line = line.transformed(linalg.rotate(angle, 'center') | linalg.translate((i, 0)))

        site += Pattern(
            dim_shape=(rotated_line, U_('µm')),
            mill=mill,
            raster_style=raster_styles.one_d.Curve(pitch=Q_('1 nm'), scan_sequence=raster_styles.ScanSequence.CONSECUTIVE)
        )

Next, some spots are added. This is done in the same manner as before. ::

    for x in np.linspace(-1, 1, 10):
        for y in np.linspace(-2, 0, 10):
            spot = shapes.Spot(position=(x, y))
            site += Pattern(
                dim_shape=(spot, U_('µm')),
                # note: the same mill as defined before can be reused but not the pattering style.
                mill=mill,
                raster_style=raster_styles.zero_d.SingleSpot()
            )

Finally, the finished pattern layout can be plotted and exported. Exporting is carried out via so called backends.
A backend takes all sites with their contained patterns and creates a file (or something else) a microscope can
understand. Currently, only two backends are publicly available in the library: first, a plotting backend for
visualization and secondly, a rasterization backend. The rasterization backend rasterizes all geometries and creates a
text file with all spots and their corresponding dwell times. ::

    dose_test_sample.plot()

    # export as text file
    dose_test_sample.export(default_backends.SpotListBackend).save('foo.txt')

The output format of the SpotListBackend can be customized and is demonstrated :ref:`here <user_guide/exporting_visualization:exporting microscope readable output>`.

The resulting plot is shown below. The blue box shows the field ov view of the patterning site. The yellow shapes
represent the patterning geometries.

Select on one of the zoom tools in the right panel to zoom in and out, either with a mouse scroll or a box selection.
Hovering over the shapes opens a pop-up menu which displays some information about the gemetrie, mill and rasterization style.

Additionally, use the measure band tool (icon with horizontal arrow) to measure distances and angles.

.. bokeh-plot-link:: ../examples/getting_started.py
    :url: https://gitlab.com/viggge/fib-o-mat/-/blob/master/examples/getting_started.py

Additionally, the script exports a file containing the rasterized patterns according to the settings given above. This file ('getting_started.txt') can be visualized with the :ref:`ion beam simulation tool <user_guide/exporting_visualization:ion beam simulation>`.

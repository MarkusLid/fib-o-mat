# Ignore the following lines. These are used to adjust the plot for the documentation.
import sys
if 'sphinx-build' in sys.argv:
    _fullscreen = False
else:
    _fullscreen = True

import numpy as np

from fibomat import Sample, Pattern, Mill, U_, Q_
from fibomat import shapes, raster_styles, linalg, default_backends


dose_test_sample = Sample(description='an optional description for yourself')

site = dose_test_sample.create_site(
    dim_position=(123, 456) * U_('µm'), dim_fov=(5, 5) * U_('µm'), description='another description'
)

# create a mill for the spots
single_repeat_mill = Mill(
    dwell_time=Q_('5 ms'), repeats=1
)

# create some spots by multiplying the (undimensioned) spot object with a unit, the spot get a defined position in the
# dimensioned coordinate system of the sites
spot = shapes.Spot([1, 2])
site.create_pattern(spot * U_('µm'), single_repeat_mill, raster_style=raster_styles.zero_d.SingleSpot())

spot = shapes.Spot([2, 2])
site.create_pattern(spot * U_('µm'), single_repeat_mill, raster_style=raster_styles.zero_d.SingleSpot())

# and a line shape
line = shapes.Line(start=(-2, 2), end=(-1, 2))

# a mill object with defines the dwell time per spot in the rasterized shape and the number of repeats
mill = Mill(
    dwell_time=Q_('5 ms'), repeats=1
)

# and finally rasterizing style. In this case, the line will be rasterized consecutive from start to end.
line_style = raster_styles.one_d.Curve(pitch=Q_('1 nm'), scan_sequence=raster_styles.ScanSequence.CONSECUTIVE)

# everything is collected in a pattern
line_pattern = Pattern(
    dim_shape=line * U_('µm'),
    mill=mill,
    raster_style=line_style
)

# and added to the site.
site += line_pattern
# or
# site.add_pattern(line_pattern)
site += line_pattern
# or
# site.add_pattern(line_pattern)

line = line.translated((0, -1))

for i, angle in enumerate(np.linspace(0, np.pi/2, 4, endpoint=False)):
    # rotate the line around its center point and translate it
    rotated_line = line.transformed(linalg.rotate(angle, 'center') | linalg.translate((i, 0)))

    site += Pattern(
        dim_shape=rotated_line * U_('µm'),
        mill=mill,
        raster_style=raster_styles.one_d.Curve(pitch=Q_('1 nm'), scan_sequence=raster_styles.ScanSequence.CONSECUTIVE)
    )

# Note: this can be done much more easily by using the layout submodule.
for x in np.linspace(-1, 1, 10):
    for y in np.linspace(-2, 0, 10):
        spot = shapes.Spot(position=(x, y))
        site += Pattern(
            dim_shape=spot * U_('µm'),
            mill=mill,
            raster_style=raster_styles.zero_d.SingleSpot()
        )


# plot the patterning layout and save the plot
# if you run the script for yourself, uncomment the following line and delete the line below.
# dose_test_sample.plot()
dose_test_sample.plot(fullscreen=_fullscreen)

# export as text file
dose_test_sample.export(default_backends.SpotListBackend).save('getting_started.txt')

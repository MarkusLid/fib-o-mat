from fibomat import Vector, U_, Q_
from fibomat.units import scale_factor, scale_to


length_unit = U_('µm')
dose_unit = U_('ions / nm**2')
print(length_unit, dose_unit)

length = Q_('1 nm')
dose = Q_('10 ions / nm**2')
another_length = 10 * length_unit
print(length, dose, another_length)

length_in_um = scale_to(U_('µm'), length)  # NOTE: length_in_um is a float now and NOT a quantity anymore
scale_factor = scale_factor(U_('µm'), U_('nm'))  # scale factor (float) to scale from nm to µm
print(length_in_um, scale_factor)

dim_vector = (Vector(1, 2), U_('µm'))

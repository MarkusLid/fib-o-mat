######
#


import numpy as np
import bokeh.plotting as bp

from fibomat import Project, Layer, Pattern
from fibomat.linalg import Vector
from fibomat.shapes import CollectionGenerator, Rect, Ellipse
from fibomat.units import U_, Q_
from fibomat.mill import AreaMill
from fibomat.default_backends import BokehBackend
import npve_backends
from npve_backends.stepandrepeat import StagePosition, StepAndRepeatListOptions


def checkerboard_pattern_rule(ix, iy):
    if iy % 2:
        return not ix % 2
    else:
        return ix % 2


def sample_outline_dict(size, center):
    size /= 2
    outer_size = size + 5
    return {
        'xs': [[[ list(map(lambda x: x+center[0], [-outer_size, outer_size, outer_size, -outer_size])), list(map(lambda x: x+center[0], [-size, size, size, -size])) ]]],
        'ys': [[[ list(map(lambda y: y+center[1], [-outer_size, -outer_size, outer_size, outer_size])), list(map(lambda y: y+center[1], [-size, -size, size, size])) ]]],
        'hatch_pattern': '/',
        'hatch_color': 'grey',
        'fill_color': None,
        'line_color': 'grey',
        'legend': 'patterning area outline'
    }


def lines_collection(line_width, pattern_size, center):
    rect = Rect(width=line_width, height=pattern_size, theta=0., center=center)
    nx = int(pattern_size / line_width)
    return CollectionGenerator(rect, nx, 1, pattern_rule=checkerboard_pattern_rule, x=(line_width, .0), y=(0., 1))

def checkerboard_collection(rect_width, pattern_size, center):
    rect = Rect(width=rect_width, height=rect_width, theta=0., center=center)
    nx = ny = int(pattern_size / rect_width)
    return CollectionGenerator(rect, nx, ny, pattern_rule=checkerboard_pattern_rule, x=(rect_width, .0), y=(0., rect_width))

def circle_collection(circle_diam, pattern_size, center):
    lattice_const = circle_diam + 1.
    circle = Ellipse(a=circle_diam, b=circle_diam, theta=0., center=center)
    nx = ny = int(pattern_size / lattice_const)
    return CollectionGenerator(
        circle, nx, ny, x=(lattice_const, .0), y=(0., lattice_const)
    )



def make_cross_patterns():
    pattern_1 = Pattern(
        shape=Rect(width=3, height=0.1, theta=0.),
        mill=AreaMill.from_dose(Q_('10 ions / nm**2'), spot_spacing_u=Q_('1. nm'), spot_spacing_v=Q_('1. nm')),
        shape_unit=U_('µm'),
        kwargs={}
    )
    pattern_2 = Pattern(
        shape=Rect(width=0.1, height=3, theta=0.),
        mill=AreaMill.from_dose(Q_('10 ions / nm**2'), spot_spacing_u=Q_('1. nm'), spot_spacing_v=Q_('1. nm')),
        shape_unit=U_('µm'),
        kwargs={}
    )

    return pattern_1, pattern_2


def save(project):
    plot: BokehBackend = project.export('bokeh', title=project.description)
    plot.plot()
    plot.show()
    xml = project.export(
        'npve_step_and_repeat',
        stage_position=StagePosition(x=0, y=0, z=0, t=0, r=0),
        options=StepAndRepeatListOptions.make_default()
    )
    xml.save(project.description + '.xml')

########################################################################################################################
# Settings
########################################################################################################################

center_focus_rect_A = Vector(500, 125)  # µm
offset_left_below_center = Vector(50, 50)

# 50 µm patterns
# membrane_size = 50
# vertical_distance_centers = 3000 # µm

# 20 µm patterns
# membrane_size = 20
# vertical_distance_centers = 1000 # µm

# 250 µm patterns
membrane_size = 250
vertical_distance_centers = 2000 # µm

name = f'{membrane_size}_um2_less_patterns_part_'

########################################################################################################################
# first move to focus point left below first membrane patch
########################################################################################################################

sp_0 = Project(name + str(0))

focus_layer_center = (
    center_focus_rect_A - Vector(0, vertical_distance_centers) - Vector(membrane_size/2, membrane_size/2)
    - offset_left_below_center
)


focus_layer = sp_0.add_layer(focus_layer_center, (10, 10), U_('µm'), 'focus point before pattern')
for pattern in make_cross_patterns():
    focus_layer.add_pattern(pattern, False)

save(sp_0)

########################################################################################################################
#  move to center of membrane patch, add pattern and move to next focus point
########################################################################################################################
if membrane_size in [20, 50]:
    for i, dose in enumerate([10, 15, 20], start=1):
        sp = Project(name + str(i))

        membrane_center = Vector(membrane_size/2, membrane_size/2) + offset_left_below_center
        membrane_layer = sp.add_layer(membrane_center, (30, 30), U_('µm'), 'patterns')

        if membrane_size == 50:
            collection = lines_collection(line_width=0.3, pattern_size=30, center=Vector.null_vector())
        else:
            collection = checkerboard_collection(rect_width=0.75, pattern_size=30, center=Vector.null_vector())

        membrane_layer.add(
            shape=collection,
            mill=AreaMill.from_dose(dose*Q_('1 ions / nm**2'), spot_spacing_u=Q_('1. nm'), spot_spacing_v=Q_('1. nm')),
            shape_unit=U_('µm')
        )

        focus_layer_center = membrane_center + Vector(1000, 0) - Vector(membrane_size/2, membrane_size/2) - offset_left_below_center
        focus_layer = sp.add_layer(focus_layer_center, (10, 10), U_('µm'), 'patterns')
        for pattern in make_cross_patterns():
            focus_layer.add_pattern(pattern, False)

        save(sp)

elif membrane_size == 250:
    ###################
    # first membrane
    ###################
    sp = Project(name + str(1))
    membrane_center = Vector(membrane_size / 2, membrane_size / 2) + offset_left_below_center

    # corner patterns
    doses_layer_centers = [
        (25, membrane_center + Vector(-105, 105)),
        (30, membrane_center + Vector(105, 105)),
        (35, membrane_center + Vector(105, -105)),
        (40, membrane_center + Vector(-105, -105)),
    ]

    for dose, layer_center in doses_layer_centers:
        membrane_layer = sp.add_layer(layer_center, (20, 20), U_('µm'))
        membrane_layer.add(
            shape=checkerboard_collection(rect_width=0.75, pattern_size=20, center=Vector.null_vector()),
            mill=AreaMill.from_dose(dose * Q_('1 ions / nm**2'), spot_spacing_u=Q_('1. nm'), spot_spacing_v=Q_('1. nm')),
            shape_unit=U_('µm')
        )
    # middle_patterns
    a = 20 + 8
    doses = np.linspace(10, 50, 5)
    widths = np.linspace(0.2, 1.5, 5)

    # middle patterns
    for j, dose in enumerate(doses, start=-int(len(doses) / 2)):
        for i, width in enumerate(widths, start=-2):
            membrane_layer = sp.add_layer(Vector(a * i, a * j) + membrane_center, (20, 20), U_('µm'))
            if width < 0.6:
                pattern_size = 10
            else:
                pattern_size = 20

            membrane_layer.add(
                shape=checkerboard_collection(rect_width=width, pattern_size=pattern_size, center=Vector.null_vector()),
                mill=AreaMill.from_dose(dose * Q_('1 ions / nm**2'), spot_spacing_u=Q_('1. nm'),
                                        spot_spacing_v=Q_('1. nm')),
                shape_unit=U_('µm')
            )

    focus_layer_center = membrane_center + Vector(1000, 0) - Vector(membrane_size / 2, membrane_size / 2) - offset_left_below_center
    focus_layer = sp.add_layer(focus_layer_center, (10, 10), U_('µm'), 'patterns')
    for pattern in make_cross_patterns():
        focus_layer.add_pattern(pattern, False)
    save(sp)

    ###################
    # second membrane
    ###################
    sp = Project(name + str(2))
    membrane_center = Vector(membrane_size / 2, membrane_size / 2) + offset_left_below_center

    # corner patterns
    for dose, layer_center in doses_layer_centers:
        membrane_layer = sp.add_layer(layer_center, (20, 20), U_('µm'))
        membrane_layer.add(
            shape=lines_collection(line_width=0.3, pattern_size=20, center=Vector.null_vector()),
            mill=AreaMill.from_dose(dose * Q_('1 ions / nm**2'), spot_spacing_u=Q_('1 nm'),
                                    spot_spacing_v=Q_('1 nm')),
            shape_unit=U_('µm')
        )

    # middle patterns
    for j, dose in enumerate(doses, start=-int(len(doses) / 2)):
        for i, width in enumerate(widths, start=-2):
            membrane_layer = sp.add_layer(Vector(a*i, a*j) + membrane_center, (20, 20), U_('µm'))
            membrane_layer.add(
                shape=lines_collection(line_width=width, pattern_size=20, center=Vector.null_vector()),
                mill=AreaMill.from_dose(dose * Q_('1 ions / nm**2'), spot_spacing_u=Q_('1 nm'),
                                        spot_spacing_v=Q_('1 nm')),
                shape_unit=U_('µm')
            )

    focus_layer_center = membrane_center + Vector(1000, 0) - Vector(membrane_size / 2,
                                                                    membrane_size / 2) - offset_left_below_center
    focus_layer = sp.add_layer(focus_layer_center, (10, 10), U_('µm'), 'patterns')
    for pattern in make_cross_patterns():
        focus_layer.add_pattern(pattern, False)
    save(sp)
    ###################
    # third membrane
    ###################
    sp = Project(name + str(3))
    membrane_center = Vector(membrane_size / 2, membrane_size / 2) + offset_left_below_center

    doses_layer_centers = [
        (45, membrane_center + Vector(-105, 105)),
        (50, membrane_center + Vector(105, 105)),
        (45, membrane_center + Vector(105, -105)),
        (50, membrane_center + Vector(-105, -105)),
    ]

    for dose, layer_center in doses_layer_centers[:2]:
        membrane_layer = sp.add_layer(layer_center, (20, 20), U_('µm'))
        membrane_layer.add(
            shape=lines_collection(line_width=0.3, pattern_size=20, center=Vector.null_vector()),
            mill=AreaMill.from_dose(dose * Q_('1 ions / nm**2'), spot_spacing_u=Q_('1. nm'),
                                    spot_spacing_v=Q_('1. nm')),
            shape_unit=U_('µm')
        )
    for dose, layer_center in doses_layer_centers[2:]:
        membrane_layer = sp.add_layer(layer_center, (20, 20), U_('µm'))
        membrane_layer.add(
            shape=checkerboard_collection(rect_width=0.75, pattern_size=20, center=Vector.null_vector()),
            mill=AreaMill.from_dose(dose * Q_('1 ions / nm**2'), spot_spacing_u=Q_('1. nm'),
                                    spot_spacing_v=Q_('1. nm')),
            shape_unit=U_('µm')
        )

    # middle patterns

    diams = np.linspace(0.05, .5, 5)

    for j, dose in enumerate(doses, start=-int(len(doses) / 2)):
        for i, diam in enumerate(diams, start=-2):
            membrane_layer = sp.add_layer(Vector(a * i, a * j) + membrane_center, (20, 20), U_('µm'))
            membrane_layer.add(
                shape=circle_collection(circle_diam=diam, pattern_size=15, center=Vector.null_vector()),
                mill=AreaMill.from_dose(dose * Q_('1 ions / nm**2'), spot_spacing_u=Q_('1. nm'),
                                        spot_spacing_v=Q_('1. nm')),
                shape_unit=U_('µm')
            )

    focus_layer_center = membrane_center + Vector(1000, 0) - Vector(membrane_size / 2,
                                                                    membrane_size / 2) - offset_left_below_center
    focus_layer = sp.add_layer(focus_layer_center, (10, 10), U_('µm'), 'patterns')
    for pattern in make_cross_patterns():
        focus_layer.add_pattern(pattern, False)
    save(sp)


else:
    raise RuntimeError
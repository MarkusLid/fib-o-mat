from fibomat.shapes import Curve, Arc, Line, Polygon, Circle
import numpy as np
from fibomat.curve_tools import offset, rasterize
from fibomat import Project
from fibomat.mill import AreaMill, SpotMill
from fibomat.units import U_, Q_
from fibomat.linalg import Vector

import npve_backends
import fibomat.default_backends


def ngon_outline_path(n_circles, circle_radius, even_odd):
    hexagon_radius = 2 * circle_radius / (2. * np.sin(np.pi / n_circles))
    hexagon = Polygon.regular_ngon(n_circles, hexagon_radius)

    curve = Curve()
    for i in range(n_circles):
        start = (hexagon.points[i % n_circles] + hexagon.points[(i + 1) % n_circles]) / 2
        end = (hexagon.points[(i + 1) % n_circles] + hexagon.points[(i + 2) % n_circles]) / 2
        if i % 2 == even_odd:
            middle = hexagon.points[(i + 1) % n_circles] + Vector(
                hexagon.points[(i + 1) % n_circles]).as_unit() * circle_radius
        else:
            middle = hexagon.points[(i + 1) % n_circles] - Vector(
                hexagon.points[(i + 1) % n_circles]).as_unit() * circle_radius

        curve.append(Arc.from_points(start, middle, end))
    return curve


def ngon_inner_path(n_circles, circle_radius):
    hexagon_radius = 2 * circle_radius / (2. * np.sin(np.pi / n_circles))
    hexagon = Polygon.regular_ngon(n_circles, hexagon_radius)

    curve = Curve()
    for i in range(n_circles):
        start = (hexagon.points[i % n_circles] + hexagon.points[(i + 1) % n_circles]) / 2
        end = (hexagon.points[(i + 1) % n_circles] + hexagon.points[(i + 2) % n_circles]) / 2
        middle = hexagon.points[(i + 1) % n_circles] - Vector(
            hexagon.points[(i + 1) % n_circles]).as_unit() * circle_radius

        curve.append(Arc.from_points(start, middle, end))
    return curve

def ngon_outer_path(n_circles, circle_radius):
    hexagon_radius = 2 * circle_radius / (2. * np.sin(np.pi / n_circles))
    hexagon = Polygon.regular_ngon(n_circles, hexagon_radius)

    curve = Curve()
    for i in range(n_circles):
        start = (hexagon.points[i % n_circles] + hexagon.points[(i + 1) % n_circles]) / 2
        end = (hexagon.points[(i + 1) % n_circles] + hexagon.points[(i + 2) % n_circles]) / 2
        middle = hexagon.points[(i + 1) % n_circles] + Vector(
            hexagon.points[(i + 1) % n_circles]).as_unit() * circle_radius

        curve.append(Arc.from_points(start, middle, end))
    return curve


def quadromer(proj, cirlce_radius: float, delta, pitch, outer_repeats, inner_repeats, layer_center):
    layer = proj.add_layer(layer_center, (1, 1), U_('µm'))

    n_circles = 4

    # inner part
    inner_curve = ngon_inner_path(n_circles, circle_radius)
    offsetted = offset([inner_curve], delta)

    while offsetted:
        for curve in offsetted:
            try:
                layer.add(
                    rasterize(curve, pitch),
                    SpotMill(dwell_time=5 * U_('µs'), repeats=inner_repeats, current=Q_('2.32 pA')), U_('µm')
                )
            except IndexError:
                pass
        offsetted = offset(offsetted, delta)


    # two curves around monomers
    curves = []
    rasterized = []

    for even_odd in [True, False]:
        curves.append(ngon_outline_path(n_circles, circle_radius, even_odd))
        rasterized.append(rasterize(curves[-1], pitch))
        print(rasterized[-1].bounding_box)

    layer.add(
        rasterized[0],
        SpotMill(dwell_time=5 * U_('µs'), repeats=outer_repeats, current=Q_('2.32 pA')), U_('µm')
    )

    layer.add(
        rasterized[1],
        SpotMill(dwell_time=5 * U_('µs'), repeats=outer_repeats, current=Q_('2.32 pA')), U_('µm')
    )

    # outer part
    outer_curve = ngon_outer_path(n_circles, circle_radius)
    offsetted = [outer_curve]
    for i in range(2):
        offsetted = offset(offsetted, -.001)
        for curve in offsetted:
            # try:
            layer.add(
                rasterize(curve, pitch),
                SpotMill(dwell_time=5 * U_('µs'), repeats=outer_repeats, current=Q_('2.32 pA')), U_('µm')
            )

# proj = Project('Hexamer')
# layer = proj.add_layer((0, 0), (1, 1), U_('µm'))

# layer.add(
#     Polygon.regular_ngon(n_circles, 2*circle_radius / (2. * np.sin(np.pi / n_circles))),
#     AreaMill.from_dose(1 * U_('coulomb / µm**2'), 1 * U_('nm'), 1 * U_('nm')), U_('µm')
# )

# curves = []
#
# rasterized = []
# # two curves around monomers
# for even_odd in [True, False]:
#     curves.append(ngon_outline_path(n_circles, circle_radius, even_odd))
#     rasterized.append(rasterize(curves[-1], pitch))
#     print(rasterized[-1].bounding_box)
#
#
# # inner part
# inner_curve = ngon_inner_path(n_circles, circle_radius)
# offsetted = offset([inner_curve], delta)
#
# while offsetted:
#     for curve in offsetted:
#         # try:
#             layer.add(
#                 rasterize(curve, pitch),
#                 SpotMill(dwell_time=5 * U_('µs'), repeats=20, current=Q_('2.32 pA')), U_('µm')
#             )
#         # except IndexError:
#         #     pass
#     offsetted = offset(offsetted, delta)
#
#
# layer.add(
#     rasterized[0],
#     SpotMill(dwell_time=5*U_('µs'), repeats=100, current=Q_('2.32 pA')), U_('µm')
# )
#
# layer.add(
#     rasterized[1],
#     SpotMill(dwell_time=5*U_('µs'), repeats=100, current=Q_('2.32 pA')), U_('µm')
# )


circle_radius = .05
# n_circles = 4
delta = 0.0025
pitch = 0.0025

outer_repeats = 100
inner_repeats = 5

proj = Project('Quadromer')

start = Vector(-3, -3)

for i in range(3):
    for j in range(3):
        quadromer(
            proj, circle_radius, delta=delta, pitch=pitch, outer_repeats=outer_repeats, inner_repeats=inner_repeats,
            layer_center=start + Vector(3*i, 3*j)
        )

# defl = proj.export('deflection_list')
# defl.save(f'grid_3x3_3um_spacing_qudromer_p_{pitch}_delta_{delta}_{inner_repeats}_repeats_inner_part_{outer_repeats}_repeats_outer_with_thick_outline.txt')

plot = proj.export('bokeh')
plot.plot()
plot.show()


import matplotlib
# matplotlib.use('Qt5Agg')
# import matplotlib.pyplot as plt
#
# plt.plot(rasterized[0].dwell_points[:,0], rasterized[0].dwell_points[:,1], 'x-')
# plt.gca().set_aspect('equal')
# plt.show()
=========
Changelog
=========


Version 0.2
===========
    * various bug fixes
    * improved documentation and a lot more examples added
    * changed smoothing algorithm
    * updated biarc approximation algorithm to handle more complicated curves
    * included more rasterization styles and cleaned up existing ones
    * SpotlistBackend can handle all existing shapes now
    * added RasterizedPattern class
    * added DimVector and DimTransformable
    * fixed layout subpackage to handle sites and patterns properly
    * included beam simulation tool


Version 0.1
===========

- A first, somehow usable version
